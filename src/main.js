import Vue from 'vue';
import App from './App.vue';
import Router from "vue-router";
import {store} from 'store';
import {router} from 'router';
import { initializeFirebase } from 'services/firebase';
import { firebaseConfig } from '../config/default';

Vue.use(Router);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});

initializeFirebase(firebaseConfig);

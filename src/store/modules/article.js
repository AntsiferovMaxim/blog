import {collection} from 'services/firebase';
import md5 from 'md5';

const state = {
  articles: [],
  currentArticle: undefined,
  isCurrentArticleFetching: false,
  isAllArticlesFetching: false
};

const getters = {
  articles(state) {
    return state.articles
  },
  currentArticle(state) {
    return state.currentArticle
  },
  isCurrentArticleFetching(state) {
    return state.isCurrentArticleFetching
  },
  isAllArticlesFetching(state) {
    return state.isAllArticlesFetching
  }
};

const mutations = {
  startAllArticlesFetching(state) {
    state.isAllArticlesFetching = true
  },
  endAllArticlesFetching(state, payload) {
    state.articles = payload;
    state.isAllArticlesFetching = false
  },
  startArticlesFetching(state) {
    state.isCurrentArticleFetching = true
  },
  endArticlesFetching(state, payload) {
    state.currentArticle = payload;
    state.isCurrentArticleFetching = false
  },
};

const actions = {
  async getArticles(ctx, page) {
    ctx.commit('startAllArticlesFetching');
    const articles = [];

    const res = await collection('articles').limit(20).page(page);

    res.forEach(item => {
      articles.push(item.data());
    });

    ctx.commit('endAllArticlesFetching', articles);
  },
  async getArticlesById(ctx, id) {
    ctx.commit('startArticlesFetching');
    const res = await collection('articles').findById(id);
    ctx.commit('endArticlesFetching', res.data());
  },
  async createArticle(ctx, obj) {
    obj = {...obj, id: md5(obj.header + Date.now())};

    collection('articles').add(obj);
  }
};

export const article = {
  state,
  getters,
  mutations,
  actions
};

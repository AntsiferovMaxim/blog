import * as firebaseService from 'services/firebase';

const state = {
  isAuth: false,
  isLoginError: false,
  isFetching: false
};

const getters = {
  isAuth: (state) => state.isAuth,
  isLoginError: (state) => state.isLoginError,
  isFetching: (state) => state.isFetching,
};

const mutations = {
  loginFetch(state) {
    state.isFetching = true;
  },
  loginSuccess(state) {
    state.isAuth = true;
    state.isFetching = false;
    state.isLoginError = false;
  },
  loginError(state) {
    state.isAuth = false;
    state.isFetching = false;
    state.isLoginError = true;
  },
  isLoginError(state, payload) {
    state.isLoginError = payload;
  },
  authLogout(state) {
    state.isAuth = false;
  }
};

const actions = {
  async signIn(ctx, payload) {
    try {
      ctx.commit('loginFetch');
      await firebaseService.signIn(payload.email, payload.password);

      const isAuth = await firebaseService.isLogin();
      if (isAuth) {
        const data = await firebaseService.getUser();
        ctx.dispatch('setUserData', data);
        ctx.commit('loginSuccess');
      }

      localStorage.setItem('autorized', 'true');
    } catch (e) {
      ctx.commit('loginError');
    }
  },
  logout(ctx){
    ctx.dispatch('clearUser');
    ctx.commit('authLogout');
    localStorage.setItem('autorized', 'false');
    firebaseService.logout();
  },
  setLoginError(ctx, error) {
    ctx.commit('isLoginError', error);
  }
};

export const auth = {
  state,
  getters,
  actions,
  mutations
};

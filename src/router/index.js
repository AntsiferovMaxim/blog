import Router from 'vue-router';
import Login from 'components/login';
import Logout from 'components/logout';
import ArticleCreator from 'components/articles-creator';
import ArticleList from 'components/articles-list';
import ArticleDetails from 'components/article-details';

export const router = new Router({
  mode: 'hash',
  routes: [{
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {title: 'Login'}
  }, {
    path: '/',
    redirect: '/all'
  }, {
    path: '/create',
    component: ArticleCreator,
    meta: {title: 'Create article'}
  }, {
    path: '/all',
    component: ArticleList,
    meta: {title: 'All articles'}
  }, {
    path: '/article/:id',
    component: ArticleDetails,
    props: true,
    meta: {title: 'Article'}
  }, {
    path: '/logout',
    component: Logout
  }]
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;

  const isAuth = localStorage.getItem('autorized') === 'true';

  if(to.path !== '/login' && !isAuth) {
    next('/login')
  } else if (to.path === '/login' && isAuth) {
    next({
      path: '/'
    })
  } else {
    next()
  }
});

import * as firebase from 'firebase';
import 'firebase/firestore';

let auth;
let database;

export function signIn(email, password) {
  return auth.signInWithEmailAndPassword(email, password)
}

export function signUp(email, password) {
  return auth.createUserWithEmailAndPassword(email, password);
}

export function logout() {
  auth.signOut();
}

export async function isLogin() {
  let isAuth;

  await auth.onAuthStateChanged(u => isAuth = u.code === undefined);

  return isAuth;
}

export async function getUser() {
  let user;

  await auth.onAuthStateChanged(u => u.code === undefined ? user = u : user = undefined);

  return user;
}

export function getCollection(collectionName) {
  return database.collection(collectionName);
}

export function collection(collectionName) {
  const collection = database.collection(collectionName);

  return {
    async findById(id) {
      const res = await collection.where('id', '==', id).get();
      let current;

      res.forEach(item => {
        current = item;
      });

      return current;
    },
    getAllByLimit(limit) {
      return collection.limit(limit).get();
    },
    add(obj) {
      return collection.add(obj);
    },
    limit(limit) {
      return {
        async page(page) {
          return collection.orderBy('header').startAfter((page - 1) * limit).limit(limit).get();
        }
      }
    }
  };
}

export function initializeFirebase (config) {
  firebase.initializeApp(config);

  auth = firebase.auth();
  database = firebase.firestore();

  return firebase;
}

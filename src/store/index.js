import Vue from "vue";
import Vuex from 'vuex';
import {user} from 'store/modules/user';
import {auth} from 'store/modules/auth';
import {article} from 'store/modules/article';

Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: false,
  modules: {
    user,
    auth,
    article
  }
});

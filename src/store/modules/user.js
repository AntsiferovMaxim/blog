const state = {
  data: {}
};

const getters = {
  userEmail(state) {
    return 'test@mail.com';//state.data.email
  }
};

const mutations = {
  updateUserData(state, data) {
    state.data = data;
  },
  clearUser(state) {
    state.data = {};
  }
};

const actions = {
  setUserData(ctx, data) {
    ctx.commit('updateUserData', data);
  },
  clearUser(ctx) {
    ctx.commit('clearUser');
  }
};

export const user = {
  state,
  getters,
  actions,
  mutations
};
